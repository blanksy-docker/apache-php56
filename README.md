# xalauc/apache-php56

## Tags

| Tag | Description |
| - | - |
| `alpha` | Experimental build, unstable not yet released, DO NOT USE |
| `1.0.0` | Version 1.0  |
| `1.0.1` | Added SQLite dependency |
| `1.0.2` `latest` | Added the www user so that the 1000 user is available for use and added sendmail |



## Setting up a project

To start the container you need to run a command like the following:

    docker run \
        --name [CONTAINER_NAME] \
        -p 80:80 \
        -p 443:443 \
        -v $(pwd)/www:/var/www \
        -v $(pwd)/sites-enabled:/etc/apache2/sites-enabled \
        -d xalauc/apache-php56

* The volume `/var/www` is the location of the web files.
* The volume `/etc/apache2/sites-enabled` is the location of any virtual hosts for the application.

## Volumes

### /var/www

Pretty self explanatory really.

### /etc/apache/sites-enabled

Place any virtual host files into this volume with an extension of `.conf` and
they will automatically be loaded into apache.

## Running commands from outside the container
To run any commands from outside the container you can format it as follows:

    docker exec -it [CONTAINER_NAME] [COMMANDS_TO_RUN]

## Accessing the container
Connecting to the virtual machine, good for checking logs and the likes,
also good for actioning commands for some applications that require command line access.

    docker exec -it [CONTAINER_NAME] bash

## License (MIT)
Copyright (c) 2016 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
