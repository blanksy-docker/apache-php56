#!/bin/bash
set -eo pipefail

for f in /docker-tools/*; do
    case "$f" in
        *.sh)     echo "$0: running $f"; . "$f" ;;
        *)        echo "$0: ignoring $f" ;;
    esac
    echo
done

apache2ctl -D FOREGROUND